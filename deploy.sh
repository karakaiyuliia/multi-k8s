docker build -t karakaiyuliia/multi-client:latest -t karakaiyuliia/multi-client:$SHA -f ./client/Dockerfile ./client
docker build -t karakaiyuliia/multi-server:latest -t karakaiyuliia/multi-server:$SHA -f ./server/Dockerfile ./server
docker build -t karakaiyuliia/multi-worker:latest -t karakaiyuliia/multi-worker:$SHA -f ./worker/Dockerfile ./worker
docker push karakaiyuliia/multi-client:latest
docker push karakaiyuliia/multi-client:$SHA
docker push karakaiyuliia/multi-server:latest
docker push karakaiyuliia/multi-server:$SHA
docker push karakaiyuliia/multi-worker:latest
docker push karakaiyuliia/multi-worker:$SHA

kubectl apply -f k8s
kubectl set image deployments/client-deployment client=karakaiyuliia/multi-client:$SHA
kubectl set image deployments/server-deployment server=karakaiyuliia/multi-server:$SHA
kubectl set image deployments/worker-deployment worker=karakaiyuliia/multi-worker:$SHA
